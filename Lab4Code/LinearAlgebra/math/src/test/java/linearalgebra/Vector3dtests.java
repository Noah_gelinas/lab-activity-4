//Noah Gelinas
package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;


public class Vector3dtests {
    
    Vector3d test=new Vector3d(1,1,2);
    Vector3d test2=new Vector3d(2,3,4);
    
    @Test
    public void shoulGetX(){
        assertEquals(1,test.getX(),0.0001);
    }

    @Test
    public void shoulGetY(){
        assertEquals(1,test.getY(),0.0001);
    }

    @Test
    public void shoulGetZ(){
        assertEquals(2,test.getZ(),0.0001);
    }

    @Test
    public void magnitudeTestMethod(){
        assertEquals(2.44948,test.magnitude(),0.00001);
    }

    @Test
    public void dotProductTest() {
        assertEquals(13,test.dotProduct(test2),0.00001);
    }

    @Test 
    public void addTest() {
        
        Vector3d test3=new Vector3d(3,4,6);
        Vector3d addTest=test.add(test2);
        assertEquals(test3.getX(),addTest.getX(),0.00001);
        assertEquals(test3.getY(),addTest.getY(),0.00001);
        assertEquals(test3.getZ(),addTest.getZ(),0.00001);
    }

}
