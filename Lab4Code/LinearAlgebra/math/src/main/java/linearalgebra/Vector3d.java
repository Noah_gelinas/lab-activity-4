//Noah Gelinas
package linearalgebra;

class Vector3d {
    private double x;
    private double y;
    private double z;
    public Vector3d(double x,double y,double z) {
        this.x=x;
        this.y=y;
        this.z=z;
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }
    public double magnitude() {
        double magnitude=Math.pow(x,2)+Math.pow(y,2)+Math.pow(z,2);
        return Math.sqrt(magnitude);
    }
    public double dotProduct(Vector3d newVector){
        double dotX=newVector.getX();
        double dotY=newVector.getY();
        double dotZ=newVector.getZ();
        return (this.x*dotX)+(this.y*dotY)+(this.z*dotZ);
    }
    public Vector3d add(Vector3d newVector) {
        double dotX=newVector.getX();
        double dotY=newVector.getY();
        double dotZ=newVector.getZ();
        double newX=this.x+dotX;
        double newY=this.y+dotY;
        double newZ=this.z+dotZ;    
        Vector3d newerVector3d=new Vector3d(newX,newY,newZ);
        return newerVector3d;
    }
    public String toString(){
       return ("("+this.x+","+this.y+","+this.z+")");
    }
}