package linearalgebra;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        Vector3d test=new Vector3d(1,1,2);
        Vector3d test2=new Vector3d(2,3,4);

        System.out.println(test.add(test2));
        System.out.println(test.dotProduct(test2));
        System.out.println(test.magnitude());
    }

    
}
